package com.pajato.isaac.core

import com.pajato.isaac.api.SerializerTest
import com.pajato.isaac.getDiagnostic
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json

class LanguageServerSpecUnitTest : SerializerTest, CoreTestProfiler() {

    @Test fun `Verify the language kind names are accessed`() {
        Language.values().let {
            assertTrue(it.contains(Language.JavaScript))
            assertTrue(it.contains(Language.Java))
            assertTrue(it.contains(Language.TypeScript))
        }
    }

    @Test fun `When invalid JSON is deserialized, verify an illegal state exception`() {
        val invalidJson = """{"streaming":"invalid"}"""
        assertFailsWith<IllegalStateException> { Json.decodeFromString(LanguageServerSpecSerializer, "xyzzy") }
        assertFailsWith<SerializationException> { Json.decodeFromString(LanguageServerSpecSerializer, invalidJson) }
    }

    @Test fun `When an invalid language server is searched, verify a correct result (null)`() {
        assertFailsWith<IllegalArgumentException> { getUniqueLanguageServerSpec("xyzzy") }
    }

    @Test fun `When a valid language server is searched, verify a correct result`() {
        val id = "KLS@fwcd1.1.1"
        val languageServer = getUniqueLanguageServerSpec(id, testRegistryPath)
        assertEquals(id, languageServer.id, getDiagnostic("language server spec"))
    }

    @Test fun `When a repeated language server is searched, verify a correct result`() {
        val id = "XLS"
        assertFailsWith<IllegalArgumentException> { getUniqueLanguageServerSpec(id, testRegistryPath) }
    }

    @Test fun `When a default language server spec is created, verify a correct result`() {
        val spec = LanguageServerSpec()
        assertEquals(Language.None, spec.language)
        assertEquals("", spec.command)
        assertEquals("", spec.id)
        assertEquals("", spec.description)
        assertEquals(null, spec.cacheDir)
        assertEquals(null, spec.sourceUrl)
    }

    @Test fun `When a non-default language server spec is created, verify a correct result`() {
        val spec = IsaacLanguageServerSpec(
            Language.Kotlin, command = "", id = "", description = "", cacheDir = "", sourceUrl = ""
        )
        assertEquals(Language.Kotlin, spec.language)
        assertEquals("", spec.command)
        assertEquals("", spec.id)
        assertEquals("", spec.description)
        assertEquals("", spec.cacheDir)
        assertEquals("", spec.sourceUrl)
    }
}
