package com.pajato.isaac.core

import com.pajato.isaac.core.client.Client
import java.io.File
import java.io.IOException
import kotlin.test.Test
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ProcessServerLauncherUnitTest : CoreTestProfiler() {

    @Test fun `When a process launcher is activated and deactivated, verify the correct behavior`() {
        Client("KLS@fwcd1.1.1", getDefaultInitializeParams(), testRegistryPath, basicCachePath).run {
            launcher.activate()
            assertTrue(launcher.active, "The server is not alive!")
            launcher.deactivate()
            assertFalse(launcher.active, "The server is still alive!")
        }
    }

    @Test fun `When a process launcher with an empty command name is accessed, verify an exception`() {
        assertFailsWith<java.lang.IllegalArgumentException> {
            Client("RouterWithEmptyCommand", getDefaultInitializeParams(), testRegistryPath, basicCachePath).launcher
        }
    }

    @Test fun `When a dirty cache miss is forced on a default client, verify the correct behavior`() {
        assertFailsWith<IOException> {
            val client = Client("KLS@fwcd1.1.1", getDefaultInitializeParams(), testRegistryPath, basicCachePath)
            setLogMap(client)
            runTestUsing(client, getKey(), 0L) { client.launcher.deactivate() }
        }
    }

    @Test fun `When a cache file error is generated (not a file), verify correct behavior`() {
        val cachePath: String = File(System.getProperty("user.home")).absolutePath
        assertFailsWith<IllegalStateException> {
            Client("KLS@fwcd1.1.1", getDefaultInitializeParams(), testRegistryPath, cachePath)
        }
    }

    @Test fun `When activating with an invalid server command, verify the correct behavior`() {
        val client = Client("KLS@fwcd0.0.0", getDefaultInitializeParams(), testRegistryPath, "")
        assertFailsWith<IllegalStateException> { client.launcher.activate() }
    }

    @Test fun `When activating with an invalid server command (script), verify the correct behavior`() {
        val client = Client("KLS@fwcd0.0.1", getDefaultInitializeParams(), testRegistryPath, "")
        assertFailsWith<IllegalStateException> { client.launcher.activate() }
    }

    @Test fun `When de-activating using a fat-jar launch, verify the correct behavior`() {
        val client = Client("KLS@fwcd1.2.0", getDefaultInitializeParams(), testRegistryPath, "")
        client.launcher.activate()
        client.launcher.deactivate()
    }
}
