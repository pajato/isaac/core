package com.pajato.isaac.core

import com.pajato.isaac.core.client.LanguageServerSpecRegistry
import kotlin.io.path.toPath

object TestLanguageServerSpecRegistry : LanguageServerSpecRegistry {
    override val persistencePath: String
    private val registry: LanguageServerSpecRegistry

    init {
        this::class.java.classLoader.getResource(".lsp/servers.txt")!!.toURI().toPath().toString().let { path ->
            persistencePath = path
            registry = LanguageServerSpecRegistry(path)
        }
    }

    override fun getAll() = registry.getAll()

    override fun find(language: Language, id: String, version: String) = registry.find(language, id, version)
}
