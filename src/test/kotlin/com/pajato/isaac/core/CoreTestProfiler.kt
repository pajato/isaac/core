package com.pajato.isaac.core

import com.pajato.isaac.ApiTestProfiler
import com.pajato.isaac.core.client.Client
import com.pajato.isaac.reportElapsedThreshold
import kotlin.test.BeforeTest
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

abstract class CoreTestProfiler : ApiTestProfiler() {
    val testRegistryPath: String by lazy { getTestResourcePath(".lsp/servers.txt") }
    val basicCachePath: String by lazy { getTestResourcePath(".lsp/StartStopExit.cache") }
    val emptyCachePath: String by lazy { getTestResourcePath(".lsp/Empty.cache") }
    private val logEntries: MutableList<String> = mutableListOf()

    fun setLogMap(client: Client) {
        client.logger.subscribe { logEntries.add(getFormattedMessage(it)) }
        testLogMap[getKey()] = logEntries
    }

    init { reportElapsedThreshold = -1L }

    @BeforeTest fun coreSetup() { testLogMap.clear() }

    internal fun getFormattedMessage(entry: LogEntry) = with(entry) {
        when {
            context.isNotEmpty() -> "$timeStamp $category in $context:\n$jsonMessage"
            else -> "$timeStamp $category:\n$jsonMessage"
        }
    }

    fun runTestUsing(
        client: Client,
        testName: String = "N/A",
        timeout: Long = defaultTimeout,
        runExtra: ((Client) -> Unit)? = null
    ) {
        fun Client.runMain() {
            fun assertServerStarted() {
                router.start(timeout, testName)
                runBlocking { delay(1L) }
                assertTrue(launcher.active, "Server has not been started!")
            }

            fun assertServerStopped() {
                router.stop(timeout)
                assertTrue(!launcher.active, "Server is still running!")
            }

            assertServerStarted()
            runExtra?.run { invoke(client) }
            assertServerStopped()
        }

        client.run {
            assertTrue(!launcher.active, "The server is already active!")
            runMain()
            assertTrue(!launcher.active, "The server did not execute the stop request message correctly!")
            assertFailsWith<IllegalStateException> { router.start(timeout, testName) }
        }
    }
}
