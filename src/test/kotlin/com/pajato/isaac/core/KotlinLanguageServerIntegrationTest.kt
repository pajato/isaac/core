package com.pajato.isaac.core

import com.pajato.isaac.core.client.Client
import kotlin.test.Test

/**
 * The basic server life-cycle consists of launch, sending an initialize request, a stop request and an exit
 * notification.
 */
class KotlinLanguageServerIntegrationTest : CoreTestProfiler() {

    private fun testUsingId(id: String) {
        val params = testClassLoader.getInitializeParams("kotlin_gradle", "basics")
        val client = Client(id, params, testRegistryPath)
        setLogMap(client)
        runTestUsing(client, getKey())
    }

    @Test fun `When using a valid Kotlin script configuration verify KLS version 1dot1dot1 basic behavior`() {
        testUsingId("KLS@fwcd1.1.1")
    }

    @Test fun `When using a valid Kotlin script configuration verify KLS version 1dot2dot0 basic behavior`() {
        testUsingId("KLS@fwcd1.2.0")
    }
}
