package com.pajato.isaac.core.client

import com.pajato.isaac.core.CoreTestProfiler
import com.pajato.isaac.core.Language
import com.pajato.isaac.core.ProcessMessageRouter
import com.pajato.isaac.core.ProcessServerLauncher
import com.pajato.isaac.core.getDefaultInitializeParams
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class ClientUnitTest : CoreTestProfiler() {
    private val params = getDefaultInitializeParams()

    @Test fun `When a default process client is created, verify a correct result`() {
        assertFailsWith<IllegalArgumentException> { ProcessClient("") }
    }

    @Test fun `When a valid process client object is created with the function constructor, verify a correct result`() {
        val client = Client("JDTLS", params, testRegistryPath)
        assertEquals(Language.Java, client.spec.language)
        assertTrue(client.launcher is ProcessServerLauncher)
        assertTrue(client.router is ProcessMessageRouter)
    }

    @Test fun `When a client object is created with the default function constructor, verify a correct result`() {
        assertFailsWith<IllegalArgumentException> { Client("xyzzy", params) }
    }

    @Test fun `When the placeholder functions are exercised, verify a correct result`() {
        assertFalse(Client("JDTLS", params, testRegistryPath).launcher.active)
        assertFalse(Client("KLS@fwcd1.1.1", params, testRegistryPath, basicCachePath).run { launcher.active })
    }
}
