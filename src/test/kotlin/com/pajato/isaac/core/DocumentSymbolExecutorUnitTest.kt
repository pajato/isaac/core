package com.pajato.isaac.core

import com.pajato.isaac.api.DocumentSymbolResult
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.core.client.Client
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class DocumentSymbolExecutorUnitTest : CoreTestProfiler() {
    private val classLoader = this::class.java.classLoader
    private val basicsKt = "sample_projects/kotlin_gradle/basics/src/main/kotlin/samples/basics.kt"
    private val documentSymbolParams = getDocumentSymbolParams(basicsKt)
    private val initializeParams = classLoader.getInitializeParams("kotlin_gradle", "basics")
    val id = "KLS@fwcd1.1.1"

    private fun assertResponse(response: ResponseMessage) {
        val result = response.result
        assertEquals(null, response.error, "The error is unexpectedly non-null!")
        assertTrue(result is DocumentSymbolResult, "The result type should be DocumentSymbolResult!")
    }

    /**
     * Use this test to refresh a cache by manually clearing the file at
     * <root-dir>/build/resources/test/.lsp/DocumentSymbol.cache prior to running the test and saving the content as
     * necessary afterwards.
     */
    @Test fun `When invoking a document symbol request, verify a correct response`() {
        val documentSymbolCachePath = getTestResourcePath(".lsp/DocumentSymbol.cache")
        val client = Client(id, initializeParams, testRegistryPath, documentSymbolCachePath)

        fun executeDocumentSymbolRequestWithDefaultTimeout() =
            DocumentSymbolExecutor(client).execute(documentSymbolParams) { message -> assertResponse((message)) }

        setLogMap(client)
        runTestUsing(client, getKey()) { executeDocumentSymbolRequestWithDefaultTimeout() }
    }
}
