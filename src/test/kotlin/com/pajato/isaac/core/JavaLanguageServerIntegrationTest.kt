package com.pajato.isaac.core

import com.pajato.isaac.core.client.Client
import kotlin.test.Ignore
import kotlin.test.Test

/**
 * The basic server life-cycle consists of launch, sending an initialize request, a stop request and an exit
 * notification.
 */
class JavaLanguageServerIntegrationTest : CoreTestProfiler() {

    @Ignore // The Eclipse JDG LS is broken.
    @Test fun `When using a valid Java configuration verify life-cycle events and log counts are correct!`() {
        runTestUsing(Client("Java", testClassLoader.getInitializeParams("java_gradle", "hello_world")), getKey())
    }
}
