package com.pajato.isaac.core

import com.pajato.isaac.api.Id
import com.pajato.isaac.api.IsaacWorkspaceSymbolResult
import com.pajato.isaac.api.ResponseError
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.core.client.Client
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking

class ProcessMessageRouterUnitTest : CoreTestProfiler() {
    private val classLoader = this::class.java.classLoader
    private val initializeParams = classLoader.getInitializeParams("kotlin_gradle", "basics")
    private val logMessages: MutableList<String> = mutableListOf()

    private fun testClient(
        id: String = "KLS@fwcd1.1.1",
        timeout: Long = 1L,
        cachePath: String = basicCachePath,
        asserter: (client: Client) -> Unit
    ) {
        Client(id, initializeParams, testRegistryPath, cachePath).run {
            val key = logger.subscribe { logMessages.add(getFormattedMessage(it)) }
            router.start(timeout, getKey())
            asserter.invoke(this)
            logger.unsubscribe(key)
        }
    }

    @Test fun `When writing a garbage message to the (un-started) server process, verify correct behavior`() {
        testClient("KLS@fwcd1.1.1") { client ->
            client.launcher.writer.write("xyzzy")
            client.launcher.writer.flush()
            client.launcher.deactivate()
        }
    }

    @Test fun `When an unregistered response message is passed to Message#routeMessage(), verify a logged error!`() {
        testClient(cachePath = emptyCachePath) { client ->
            ResponseMessage(Id.NumberId(12), null, null, "no such json version").routeMessage(client.router)
            assertEquals(8, logMessages.size, "The number of log entries is wrong!")
        }
    }

    @Test fun `When a low initialize timeout is specified, verify the router is not active`() {
        testClient(cachePath = emptyCachePath) { assertTrue(it.router.initializeResult == null) }
    }

    @Test fun `When starting out, verify that the next request id starts at 1!`() {
        testClient { client -> assertEquals(2, client.router.getNextRequestId().toInt()) }
    }

    @Test fun `When incrementing the next request id, verify correct behavior`() {
        testClient { client ->
            client.router.incrementNextRequestId()
            assertEquals(3, client.router.getNextRequestId().toInt())
        }
    }

    @Test fun `When the initialize result is accessed before a successful initialize request, verify the result`() {
        testClient(cachePath = emptyCachePath) { assertTrue(it.router.initializeResult == null) }
    }

    @Test fun `When a default response message is generated, verify the result`() {
        val idValue = 546
        val response = ResponseMessage(id = Id.NumberId(idValue))
        val id = response.id

        fun assertProcessResultFails() {
            testClient { client -> assertFailsWith<IllegalStateException> { response.processResult(client.router) } }
        }

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(null, response.result, "The default result value is wrong!")
        assertEquals(null, response.error, "The default error value is wrong!")
        assertProcessResultFails()
    }

    @Test fun `When a response message with a workspace symbol result is generated, verify correct behavior`() {
        val idValue = 546
        val result = IsaacWorkspaceSymbolResult(listOf())
        val response = ResponseMessage(id = Id.NumberId(idValue), result = result)
        val id = response.id

        fun assertProcessResultSucceeds() { testClient { client -> response.processResult(client.router) } }

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(result, response.result, "The default result value is wrong!")
        assertEquals(null, response.error, "The default error value is wrong!")
        assertProcessResultSucceeds()
    }

    @Test fun `When a response message with an error is produced, verify correct behavior`() {
        val idValue = 546
        val errorCode = -12
        val errorMessage = "xyzzy"
        val responseError = ResponseError(errorCode, errorMessage)
        val response = ResponseMessage(id = Id.NumberId(idValue), error = responseError)
        val id = response.id
        val error = response.error

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(null, response.result, "The default result value is wrong!")
        assertTrue(error is ResponseError)
        assertEquals(errorCode, error.code, "The default error code is wrong!")
        assertEquals(errorMessage, error.message, "The default error message is wrong!")
    }

    @Test fun `When using a negative timeout, verify correct behavior`() {
        testClient(timeout = -1L) { client ->
            var elapsedTime = 0L
            val timeout = 100L
            assertTrue(client.router.initializeResult == null)
            runBlocking {
                while (elapsedTime++ < timeout) { if (client.router.initializeResult != null) break else delay(1L) }
                assertTrue(client.router.initializeResult != null, "The initialize result should not be null!")
            }
        }
    }
}
