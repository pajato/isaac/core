package com.pajato.isaac.core

import com.pajato.isaac.core.client.Client
import kotlin.test.Ignore
import kotlin.test.Test

/**
 * The basic server life-cycle consists of launch, sending an initialize request, a stop request and an exit
 * notification.
 */
class GroovyLanguageServerIntegrationTest : CoreTestProfiler() {

    @Ignore // This test needs to be converted to a "fast" test, a low priority item.
    @Test fun `When using a valid Groovy configuration verify life-cycle events and log counts are correct!`() {
        runTestUsing(Client("GLS", testClassLoader.getInitializeParams("groovy", "hello_world.groovy")), getKey())
    }
}
