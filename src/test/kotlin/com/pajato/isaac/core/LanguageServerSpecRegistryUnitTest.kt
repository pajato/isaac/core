package com.pajato.isaac.core

import com.pajato.isaac.core.client.LanguageServerSpecRegistry
import com.pajato.isaac.core.client.defaultServerSpecPath
import com.pajato.isaac.getDiagnostic
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

internal class LanguageServerSpecRegistryUnitTest : CoreTestProfiler() {

    @Test fun `When invoking the default language server registry, verify a correct result`() {
        val registry = LanguageServerSpecRegistry()
        assertTrue(registry.persistencePath.endsWith("/.lsp/servers.txt"), getDiagnostic("default server registry"))
    }

    @Test fun `When creating a new language server registry, verify it is empty`() {
        assertFalse(TestLanguageServerSpecRegistry.getAll().isEmpty(), "A new servers file unexpectedly has entries!")
    }

    @Test fun `When able to find a suitable entry, verify a correct result`() {
        assertTrue(TestLanguageServerSpecRegistry.find(Language.Kotlin, "KLS@fwcd1.1.1", "").isNotEmpty())
    }

    @Test fun `When unable to find a suitable entry, verify a correct result`() {
        assertTrue(TestLanguageServerSpecRegistry.find(Language.Kotlin, "KLS@fwcd1.1.1", "0").isEmpty())
    }

    @Test fun `When using the default file path to create a language server spec registry, verify correct behavior`() {
        val file = File(defaultServerSpecPath)
        if (file.exists()) assertEquals(defaultServerSpecPath, LanguageServerSpecRegistry().persistencePath)
    }

    @Test fun `When using a bogus file path to create a language server spec registry, verify correct behavior`() {
        assertFailsWith<IllegalArgumentException> { LanguageServerSpecRegistry("/xyzzy") }
    }
}
