package com.pajato.isaac.core

import com.pajato.isaac.clientRequestTag
import com.pajato.isaac.getDiagnostic
import com.pajato.isaac.serverNotificationTag
import com.pajato.isaac.serverResponseTag
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlinx.coroutines.runBlocking

class LoggerUnitTest : CoreTestProfiler() {
    private lateinit var logger: Logger
    private var key: Int = -1
    private val logMessages: MutableList<String> = mutableListOf()

    @BeforeTest fun loggerSetUp() {
        logger = CoreLogger()
        key = logger.subscribe { logMessages.add(it.jsonMessage) }
    }

    @AfterTest fun loggerTearDown() {
        logger.unsubscribe(key)
    }

    @Test fun `When creating a default core logger object, verify no items yet`() {
        assertEquals(0, logMessages.size, getDiagnostic("log entries size"))
    }

    @Test fun `When adding an entry to a default core logger object, verify the result`() {
        logger.logMessage("someCategory", "a message ...", "a subsystem scope")
        assertEquals(1, logMessages.size, getDiagnostic("log entries size"))
    }

    @Test fun `When adding an entry with no scope to a default core logger object, verify the result`() {
        logger.logMessage("someCategory", "a message ...")
        assertEquals(1, logMessages.size, getDiagnostic("log entries size"))
    }

    @Test fun `When a logger with a single subscriber is used, verify correct behavior`() = runBlocking {
        val messageList: MutableList<LogEntry> = mutableListOf()
        logger.subscribe { messageList.add(it) }

        logger.logMessage(clientRequestTag, "client requests a response from server", "a context")
        logger.logMessage(serverNotificationTag, "server notifies client", "ProcessMessageRouter")
        logger.logMessage(serverResponseTag, "server responds to client request", "ProcessMessageRouter")

        assertEquals(3, messageList.size)
    }

    @Test fun `When a logger with multiple subscribers is used, verify correct behavior`() = runBlocking {
        val messageList1: MutableList<LogEntry> = mutableListOf<LogEntry>().apply { logger.subscribe { this.add(it) } }
        val messageList2: MutableList<LogEntry> = mutableListOf<LogEntry>().apply { logger.subscribe { this.add(it) } }
        val messageList3: MutableList<LogEntry> = mutableListOf<LogEntry>().apply { logger.subscribe { this.add(it) } }

        logger.logMessage(clientRequestTag, "client requests a response from server", "a context")
        logger.logMessage(serverNotificationTag, "server notifies client", "ProcessMessageRouter")
        logger.logMessage(serverResponseTag, "server responds to client request", "ProcessMessageRouter")

        assertEquals(4, (logger as CoreLogger).handlerMap.size)
        assertEquals(3, messageList1.size)
        assertEquals(3, messageList2.size)
        assertEquals(3, messageList3.size)
        repeat(3) { logger.unsubscribe(it) }
        assertEquals(1, (logger as CoreLogger).handlerMap.size)
    }
}
