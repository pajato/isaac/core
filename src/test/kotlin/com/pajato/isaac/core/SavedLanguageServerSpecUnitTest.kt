package com.pajato.isaac.core

import com.pajato.isaac.api.SerializerTest
import java.io.File
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.test.fail

internal const val missingSpecs = "The language server spec registry is missing!"

class SavedLanguageServerSpecUnitTest : SerializerTest, CoreTestProfiler() {

    @Test fun `When the saved language server instances are deserialized, verify they are valid`() {
        val registry = mutableSetOf<String>()
        val path: String = testClassLoader.getResource(".lsp/servers.txt")?.path ?: fail(missingSpecs)

        fun doAsserts(json: String) {
            fun doAsserts(spec: LanguageServerSpec) {
                val id = spec.id
                assertTrue(spec.id.isNotEmpty(), "There is a language server spec entry with an empty ID")
                assertFalse(registry.contains(id), "There are duplicate language server spec entries with ID: $id!")
            }

            testDecode(json, LanguageServerSpecSerializer, ::doAsserts)
        }

        File(path).readLines().forEach(::doAsserts)
    }
}
