package com.pajato.isaac.core

import com.pajato.isaac.core.client.Client
import com.pajato.isaac.getDiagnostic
import java.io.File
import kotlin.test.AfterTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class MessageCacheUnitTest : CoreTestProfiler() {
    val id = "KLS@fwcd1.1.1"
    lateinit var client: Client
    private val params = testClassLoader.getInitializeParams("kotlin_gradle", "basics")

    @AfterTest fun cachingTestTearDown() {
        if (this::client.isInitialized) with(client.launcher) { if (active) deactivate() }
    }

    @Test fun `When using a caching message router, verify the correct behavior`() {
        fun assertRouterAndLauncher() {
            val launcher = client.launcher as ProcessServerLauncher
            assertTrue(client.router is ProcessMessageRouter)
            assertTrue(client.launcher is ProcessServerLauncher)
            assertTrue(File(client.cachePath).exists())
            assertTrue(launcher.cache.isNotEmpty())
        }

        client = Client(id, params, testRegistryPath, basicCachePath)
        setLogMap(client)
        runTestUsing(client, getKey(), 500L)
        assertRouterAndLauncher()
    }

    @Test fun `When the empty cache is loaded, verify correct behavior`() {
        check(File(emptyCachePath).readText().isEmpty()) { "The empty cache file is not empty!" }
        client = Client(id, params, testRegistryPath, emptyCachePath)
        val launcher = client.launcher as ProcessServerLauncher
        assertTrue(launcher.cache.isEmpty(), getDiagnostic("cache size"))
        assertFalse(launcher.useCache, getDiagnostic("use cache"))
    }

    /**
     * Use this test to refresh a cache by manually clearing the file at
     * <root-dir>/build/resources/test/.lsp/StartStopExit.cache prior to running the test and saving the content as
     * necessary afterwards.
     */
    @Test fun `When generating cache content for a start, stop, exit cycle, verify correct behavior`() {
        client = Client(id, params, testRegistryPath, basicCachePath).also { client ->
            val cache = MessageCache(client).apply { clear() }
            setLogMap(client)
            client.logger.subscribe { cache.addEntryToCache(it) }
            runTestUsing(client, getKey())
        }
    }

    @Test fun `When creating a message cache, verify the correct behavior`() {
        client = Client(id, params, testRegistryPath, emptyCachePath).also {
            val cache = MessageCache(it).apply { clear() }
            assertEquals(emptyCachePath, cache.cacheFile.absolutePath)
        }
    }

    @Test fun `When adding entries to a copied message cache, verify the correct behavior`() {
        client = Client(id, params, testRegistryPath, getTestResourcePath(".lsp/MultipleEntry.cache"))
        MessageCache(client).copy()
        assertEquals(3, ProcessServerLauncher(client).cache.size)
    }

    @Test fun `When copying to a non-existent directory, verify correct behavior`() {
        val logEntries: MutableList<LogEntry> = mutableListOf()
        client = Client("InvalidCacheDir", params, testRegistryPath, emptyCachePath)
        client.logger.subscribe { logEntries.add(it) }
        MessageCache(client).copy()
        assertEquals(1, logEntries.size)
    }

    @Test fun `When setting up a cache with a single request message, verify correct behavior`() {
        client = Client(id, params, testRegistryPath, getTestResourcePath(".lsp/SingleEntry.cache"))
        assertEquals(1, ProcessServerLauncher(client).cache.size)
    }

    @Test fun `When setting up a cache with multiple request messages, verify correct behavior`() {
        client = Client(id, params, testRegistryPath, basicCachePath)
        assertEquals(3, ProcessServerLauncher(client).cache.size)
    }

    @Test fun `When an invalid cache (more than three parts) is presented, verify correct behavior`() {
        assertFailsWith<IllegalStateException> {
            Client(id, params, testRegistryPath, getTestResourcePath(".lsp/Invalid.cache"))
        }
    }
}
