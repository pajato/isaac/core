package com.pajato.isaac.core

import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.ClientInfo
import com.pajato.isaac.api.DocumentSymbolParams
import com.pajato.isaac.api.DocumentUri
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.api.TextDocumentIdentifier
import com.pajato.isaac.api.Trace
import com.pajato.isaac.api.WorkspaceFolder
import kotlin.test.fail

const val appName = "Test App"
const val appVersion = "1.0-SNAPSHOT"

val testClassLoader: ClassLoader = CoreTestProfiler::class.java.classLoader

fun getTestResourcePath(resource: String) = testClassLoader.getResource(resource)?.path
    ?: fail("Can not configure the resource with the relative path: $resource!")

internal fun getDocumentSymbolParams(relativePath: String) =
    DocumentSymbolParams(textDocument = TextDocumentIdentifier("file://${getTestResourcePath(relativePath)}"))

internal fun ClassLoader.getTestProject(projectDir: String, projectName: String): DocumentUri {
    val path = "sample_projects/$projectDir/$projectName"
    val resource = this.getResource(path)
    return resource?.toURI()?.toString() ?: """Error: The project with path "$path" is not supported (not found)!"""
}

internal fun ClassLoader.getInitializeParams(resourceDir: String, projectName: String): InitializeParams {
    val defaultParams = InitializeParams(
        trace = Trace.verbose, clientInfo = ClientInfo(appName, appVersion), capabilities = ClientCapabilities(),
        workspaceFolders = listOf(WorkspaceFolder(getTestProject(resourceDir, projectName), projectName))
    )

    return when (resourceDir) {
        "kotlin_gradle" -> defaultParams
        "java_gradle" -> defaultParams
        "groovy" -> defaultParams
        else -> throw IllegalStateException("Unsupported resource directory: $resourceDir!")
    }
}
