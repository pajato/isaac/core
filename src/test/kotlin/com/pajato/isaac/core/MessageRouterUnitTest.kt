package com.pajato.isaac.core

import com.pajato.isaac.api.Id
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.NotificationMessage
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.ResponseError
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.core.client.Client
import java.io.Reader
import java.io.StringReader
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertTrue

class MessageRouterUnitTest : CoreTestProfiler() {
    private val idPattern = """:(\d+),"result":"""

    @Test fun `When an invalid request message is routed, verify an illegal state exception!`() {
        val requestMessage = RequestMessage(Id.NumberId(12), "shutdown", null, "no such json version")
        val params: InitializeParams = getDefaultInitializeParams()
        Client("KLS@fwcd1.1.1", params, testRegistryPath, basicCachePath).run {
            assertFailsWith<IllegalStateException> { requestMessage.routeMessage(router) }
        }
    }

    @Test fun `When a valid result is presented for toId(), verify the correct return value`() {
        val message = """:6,"result":"""
        val id = idPattern.toId(message)
        assertEquals(6, id)
    }

    @Test fun `When an invalid result is presented for toId(), verify the correct return value`() {
        val message = "not a valid json string"
        assertFailsWith<IllegalStateException> { idPattern.toId(message) }
    }

    @Test fun `When the command name for a process activation is empty, verify an illegal state exception`() {
        val id = "SpecWithEmptyCommand"
        assertFailsWith<IllegalStateException> {
            ProcessServerLauncher(Client(id, getDefaultInitializeParams(), testRegistryPath))
        }
    }

    @Test fun `When executeIfNotEmpty() is invoked on the empty string, verify coverage`() {
        StringBuilder().executeIfNotEmpty { println("Never gets executed because receiver is empty.") }
    }

    @Test fun `When a default response message is generated, verify the result`() {
        val idValue = 546
        val response = ResponseMessage(id = Id.NumberId(idValue))
        val id = response.id

        fun assertProcessResultFails() {
            Client("KLS@fwcd1.1.1", getDefaultInitializeParams(), testRegistryPath, basicCachePath).router.let {
                assertFailsWith<IllegalStateException> { response.processResult(it) }
            }
        }

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(null, response.result, "The default result value is wrong!")
        assertEquals(null, response.error, "The default error value is wrong!")
        assertProcessResultFails()
    }

    @Test fun `When a response message with an error result is generated, verify correct behavior`() {
        val idValue = 546
        val errorCode = -12
        val errorMessage = "xyzzy"
        val responseError = ResponseError(errorCode, errorMessage)
        val response = ResponseMessage(id = Id.NumberId(idValue), error = responseError)
        val id = response.id
        val error = response.error

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(null, response.result, "The default result value is wrong!")
        assertTrue(error is ResponseError)
        assertEquals(errorCode, error.code, "The default error code is wrong!")
        assertEquals(errorMessage, error.message, "The default error message is wrong!")
    }

    @Test fun `When a response message with both error and result null, verify correct behavior`() {
        val idValue = 546
        val response = ResponseMessage(id = Id.NumberId(idValue), result = null, error = null)
        val id = response.id

        assertTrue(id is Id.NumberId, "The id should a NumberId object!")
        assertEquals(idValue, id.id, "The id value is wrong!")
        assertEquals(null, response.result, "The default result value is wrong!")
        assertEquals(null, response.error, "The default error value is wrong!")
    }

    @Test fun `When an invalid response message is used to process a result, verify correct behavior`() {
        val idValue = 546
        val errorCode = -12
        val errorMessage = "xyzzy"
        val responseError = ResponseError(errorCode, errorMessage)
        val response = ResponseMessage(id = Id.NumberId(idValue), error = responseError)

        Client("KLS@fwcd1.1.1", getDefaultInitializeParams(), testRegistryPath).router.let { router ->
            response.processResult(router)
            assertTrue(router.initializeResult == null)
        }
    }

    @Test fun `When getting a negative content length message from a reader stream, verify correct behavior`() {
        val reader: Reader = StringReader("")
        val buffer = StringBuilder()
        assertFailsWith<IllegalStateException> { getMessagesFromStreamUsing(reader, buffer) { } }
    }

    @Test fun `When default start and stop operations are invoked, verify correct behavior`() {
        TestMessageRouter().apply {
            start()
            stop()
        }
    }

    @Test fun `Verify a mutated initializeResult is correct`() {
        TestMessageRouter().apply { initializeResult = null }
    }

    inner class TestMessageRouter : MessageRouter, Logger by CoreLogger() {
        override var initializeResult: InitializeResult?
            get() = TODO("Not yet implemented")
            set(value) { assertEquals(null, value) }

        override val responseHandlerMap: MutableMap<Int, (ResponseMessage) -> Unit> get() = TODO("Not yet implemented")

        override fun getNextRequestId(): Id = Id.NumberId(2)

        override fun incrementNextRequestId() { TODO("Not yet implemented") }

        override fun sendRequest(request: RequestMessage, responseHandler: ResponseHandler): Id =
            request.id.apply { assertEquals(2, request.id.toInt()) }

        override fun sendNotification(notification: NotificationMessage) { TODO("Not yet implemented") }

        override fun start(timeout: Long, vararg args: String) { assertEquals(defaultTimeout, timeout) }

        override fun stop(timeout: Long) { assertEquals(defaultTimeout, timeout) }

        override suspend fun waitForConditionOrTimeout(
            timeout: Long,
            id: Id,
            method: String,
            condition: () -> Boolean
        ): Boolean { return true }
    }
}
