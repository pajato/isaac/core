package com.pajato.isaac.core

import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.core.client.Client
import com.pajato.isaac.getDiagnostic
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class CoreSupportUnitTest : CoreTestProfiler() {
    val id = "KLS@fwcd1.1.1"
    private val params = InitializeParams(capabilities = ClientCapabilities())
    lateinit var client: Client

    @BeforeTest fun coreUtilsSetup() {
        client = Client(id, params, testRegistryPath, basicCachePath)
        client.launcher.activate()
    }

    @AfterTest fun coreUtilsTearDown() { client.launcher.deactivate() }

    @Test fun `When obtaining a unique router using a non-existent id, verify a correct result`() {
        assertFailsWith<IllegalArgumentException> { Client("noSuchServerSpec", params, testRegistryPath) }
    }

    @Test fun `When obtaining a bogus language server, verify a correct result`() {
        assertFailsWith<IllegalArgumentException> { getUniqueLanguageServerSpec("noSuchServer", testRegistryPath) }
    }

    @Test fun `When activating a client with an invalid registry path, verify the correct behavior`() {
        val id = "someId"
        val path = "/"
        val exc = Exception("""The persistence file: $path must be a file!""")
        val params = InitializeParams(capabilities = ClientCapabilities())
        var message = ""
        val noMessageAvailable = "No message available unexpectedly!"

        try { Client(id, params, path) } catch (exc: Exception) { message = exc.message ?: noMessageAvailable }

        assertEquals(exc.message, message, getDiagnostic("client diagnostic"))
    }
}
