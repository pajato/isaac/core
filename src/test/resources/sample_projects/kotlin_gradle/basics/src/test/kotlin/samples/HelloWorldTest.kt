@file:Suppress("PackageDirectoryMismatch")

package sample_projects.kotlin_gradle.basic_hello_world.src.test.kotlin.samples

import kotlin.test.Test
import sample_projects.kotlin_gradle.basic_hello_world.src.main.kotlin.samples.main

class HelloWorldTest {

    @Test
    fun `When the most basic variant of the Kotlin hello world program is run, visually verify the result`() {
        main()
    }
}
