@file:Suppress("unused", "PackageDirectoryMismatch", "KotlinConstantConditions", "KotlinConstantConditions",
    "KotlinConstantConditions", "KotlinConstantConditions", "KotlinConstantConditions"
)

package samples

fun mainWithNoParameters() {
    val message = "Hello World!"
    println(message)
    var counter = 0
    val foo = if (counter % 2 == 0) "even" else "odd"
    when (counter++) {
        1 -> println("Is one")
        2 -> println("Is two")
        else -> println("Is $counter and foo is $foo")
    }
}

fun mainWithNoInitializers(param: String) {
    println(param)
}

fun mainWithInitializer(param: String = "initialized") {
    println(param)
}

val prop = mainWithInitializer()
