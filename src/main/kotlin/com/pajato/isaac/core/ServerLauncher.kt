package com.pajato.isaac.core

import java.io.Reader
import java.io.Writer

interface ServerLauncher {
    /**
     * True if the server has been activated, false otherwise.
     */
    val active: Boolean

    /**
     * The streaming reader from which response and notification messages are read.
     */
    val reader: Reader

    /**
     * The streaming writer into which request and notification messages are written.
     */
    val writer: Writer

    /**
     * Activate the launcher
     */
    fun activate(vararg extraArgs: String)

    /**
     * Deactivate the launcher
     */
    fun deactivate()
}
