package com.pajato.isaac.core

import com.pajato.isaac.api.Id
import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.NotificationMessage
import com.pajato.isaac.api.NotificationMessageSerializer
import com.pajato.isaac.api.NumberParams
import com.pajato.isaac.api.Params
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.RequestMessageSerializer
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.clientNotificationTag
import com.pajato.isaac.clientRequestTag
import com.pajato.isaac.core.client.Client
import com.pajato.isaac.getInitializeRequestMessage
import com.pajato.isaac.getShutdownRequestMessage
import com.pajato.isaac.serverErrorTag
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.Json

class ProcessMessageRouter(val client: Client, val launcher: ServerLauncher = client.launcher) :
    MessageRouter, Logger by client.logger {
    override var initializeResult: InitializeResult? = null
    override val responseHandlerMap: MutableMap<Int, (ResponseMessage) -> Unit> = mutableMapOf()

    private var nextRequestId = 1
    override fun getNextRequestId() = Id.NumberId(nextRequestId)
    override fun incrementNextRequestId() { nextRequestId++ }

    override fun sendNotification(notification: NotificationMessage) {
        val notificationAsJson = Json.encodeToString(NotificationMessageSerializer, notification)
        val header = "Content-Length: ${notificationAsJson.length}\r\n\r\n"
        val message = "$header$notificationAsJson"
        startLoggedRequest(clientNotificationTag, message, client.spec.id)
    }

    private fun startLoggedRequest(tag: String, message: String, name: String) {
        logMessage(tag, message, name)
        client.launcher.writer.write(message)
        client.launcher.writer.flush()
    }

    override fun sendRequest(request: RequestMessage, responseHandler: ResponseHandler): Id =
        Json.encodeToString(RequestMessageSerializer, request).let { message ->
            startLoggedRequest(clientRequestTag, "Content-Length: ${message.length}\r\n\r\n$message", client.spec.id)
            responseHandlerMap[request.id.toInt()] = responseHandler
            nextRequestId++
            request.id
        }

    override fun start(timeout: Long, vararg args: String) {
        fun sendInitializeRequestMessage() =
            sendRequest(getInitializeRequestMessage(client.params)) { response -> response.processResult(this) }

        check(initializeResult == null) { "Router ${client.spec.id} is started already and can only be started once!" }
        client.launcher.activate(*args)
        client.cancelRequestMaybe(timeout, sendInitializeRequestMessage(), method = "initialize") {
            initializeResult != null
        }
    }

    override fun stop(timeout: Long) {
        fun sendShutdownRequestMessage() = sendRequest(getShutdownRequestMessage(getNextRequestId().toInt())) {
            val message = """Shutdown response: Shutting down language server ${client.spec.id}."""
            val notification = NotificationMessage("exit", NumberParams(200))

            logMessage(clientInfoTag, message, "sendShutdownRequestMessage")
            runBlocking { sendNotification(notification); delay(5L) }
            client.launcher.deactivate()
        }

        client.cancelRequestMaybe(timeout, sendShutdownRequestMessage(), method = "shutdown") {
            !client.launcher.active
        }
    }

    override suspend fun waitForConditionOrTimeout(
        timeout: Long,
        id: Id,
        method: String,
        condition: () -> Boolean
    ): Boolean {
        var elapsedTime = 0L
        var conditionIsSatisfied = condition()

        fun cancelRequest() {
            val subsystem = "waitForConditionOrTimeout"
            val message = "$method request message (id: ${id.toInt()} canceled, reason: timeout ($timeout ms)!"
            val params: Params = NumberParams(id.toInt())
            val notification = NotificationMessage("$/cancelRequest", params)

            logMessage(clientInfoTag, message, subsystem)
            sendNotification(notification)
        }

        suspend fun checkForConditionSatisfied(): Boolean {
            if (conditionIsSatisfied) return true
            delay(delayInMilliseconds)
            elapsedTime += delayInMilliseconds
            conditionIsSatisfied = condition()
            return conditionIsSatisfied
        }

        while (timeout > elapsedTime) if (checkForConditionSatisfied()) return true
        if (!conditionIsSatisfied) cancelRequest()
        return conditionIsSatisfied
    }
}

internal fun Client.cancelRequestMaybe(timeout: Long, id: Id, method: String, condition: () -> Boolean) {
    fun sendCancelRequestNotification() {
        val message = "$method request message (id: ${id.toInt()} canceled, reason: timeout ($timeout ms)!"
        logger.logMessage(clientInfoTag, message, "cancelRequestMaybe()")
        router.sendNotification(NotificationMessage("$/cancelRequest", NumberParams(id.toInt())))
    }

    fun waitForResult() = runBlocking {
        val timedOut = !router.waitForConditionOrTimeout(timeout, id, method, condition)

        fun cancelRequest() {
            val message = "The $method request timed out and is being canceled!"
            logger.logMessage(serverErrorTag, message, "sendRequest()")
            sendCancelRequestNotification()
        }

        if (timedOut) cancelRequest()
    }

    when {
        timeout < 0L -> return
        timeout == 0L -> sendCancelRequestNotification()
        else -> waitForResult()
    }
}
