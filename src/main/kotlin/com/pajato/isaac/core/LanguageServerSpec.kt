package com.pajato.isaac.core

import com.pajato.isaac.core.client.LanguageServerSpecRegistry
import com.pajato.isaac.core.client.defaultServerSpecPath
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.JsonObject

interface LanguageServerSpec {
    /**
     * The language kind (Kotlin, Java, Julia, Lisp, etc.)
     */
    val language: Language

    /**
     * The command used to invoke the language server, or as a discriminant, as needed.
     */
    val command: String

    /**
     * An identifier for the LS.
     */
    val id: String

    /**
     * The language server description; a placeholder for arbitrary, free form text.
     */
    val description: String

    /**
     * The path to a directory containing cache files for a given server spec. By default, the path ".lsp/" in the
     * User's home directory is used when the value is null or no value is provided.
     */
    val cacheDir: String?

    /**
     * The source code URL, a non-empty string, if available.
     */
    val sourceUrl: String?
}

fun LanguageServerSpec(
    language: Language = Language.None,
    command: String = "",
    id: String = "",
    description: String = "",
    cacheDir: String? = null,
    sourceUrl: String? = null,
): LanguageServerSpec = IsaacLanguageServerSpec(language, command, id, description, cacheDir, sourceUrl)

@Serializable class IsaacLanguageServerSpec(
    override val language: Language,
    override val command: String,
    override val id: String,
    override val description: String,
    override val cacheDir: String? = null,
    override val sourceUrl: String? = null
) : LanguageServerSpec

object LanguageServerSpecSerializer : JsonContentPolymorphicSerializer<LanguageServerSpec>(LanguageServerSpec::class) {
    override fun selectDeserializer(element: JsonElement): DeserializationStrategy<out LanguageServerSpec> {
        check(element is JsonObject) { "Must be a Json element." }
        return IsaacLanguageServerSpec.serializer()
    }
}

/**
 * Attempt to get a unique language server spec throwing an illegal argument exception if the attempt fails.
 *
 * @param uniqueId is the unique id (hopefully) associated with the spec.
 * @param path is the absolute file path to the registry containing server specs.
 *
 * @return is the language server spec associated with the given unique id.
 *
 * @throws IllegalArgumentException when the unique id is invalid (not unique) or the path is invalid.
 */
fun getUniqueLanguageServerSpec(uniqueId: String, path: String = defaultServerSpecPath): LanguageServerSpec {
    val registry = LanguageServerSpecRegistry(path)
    val list = registry.getAll().filter { it.id == uniqueId }

    fun getResultOrThrow() = when (list.size) {
        0 -> throw IllegalArgumentException("""Found no items with id: "$uniqueId" at path: "$path"!""")
        1 -> list[0]
        else -> throw IllegalArgumentException("""Found ${list.size} specs with id: "$uniqueId" at path: "$path"!""")
    }

    return getResultOrThrow()
}
