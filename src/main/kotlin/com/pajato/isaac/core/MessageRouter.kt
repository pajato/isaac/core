package com.pajato.isaac.core

import com.pajato.isaac.api.Id
import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.NotificationMessage
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.ResponseMessage

internal const val defaultTimeout: Long = 25000L
internal const val contentRE = "^Content-Length: (\\d+)\r\n\r\n"

/**
 * A message router is responsible for routing JSON RPC messages between a client and a server. Notification messages
 * are typically sent in both directions, request messages mainly are sent from the client to the server and mandatory
 * response messages are sent from the server to the client whenever a request is received by the server.
 */
interface MessageRouter : Logger {
    /**
     * The result from a successful language server launch. It includes the server name, version and capabilities.
     */
    var initializeResult: InitializeResult?

    /**
     * A map associating a request id with its response message handler.
     */
    val responseHandlerMap: MutableMap<Int, (ResponseMessage) -> Unit>

    /**
     * Get the next request id value.
     *
     * @return the current value for the next request message.
     */
    fun getNextRequestId(): Id

    /**
     * Increment the next request id value.
     */
    fun incrementNextRequestId()

    /**
     * Sends a client request message to the server for processing.
     *
     * @param request is the message to be processed by the server.
     * @param responseHandler is a lambda response handler that will be invoked when a response is returned.
     *
     * @return the request message id per the LSP specification.
     */
    fun sendRequest(request: RequestMessage, responseHandler: ResponseHandler): Id

    /**
     *
     */
    fun sendNotification(notification: NotificationMessage)

    /**
     * Start the server process passing extra command line arguments, if any.
     *
     * @param timeout is the number of milliseconds the client router will wait before declaring a timeout exception.
     * If negative, the client router will not check for timeout (fire and forget). The default is 25 seconds.
     */
    fun start(timeout: Long = defaultTimeout, vararg args: String)

    /**
     * Stop the running server.
     *
     * @param timeout if non-negative, the client router will wait that many milliseconds before killing the server
     * process. If negative, the action is fire and forget.
     */
    fun stop(timeout: Long = defaultTimeout)

    /**
     * A response timeout detector.
     *
     * @param timeout the number of milliseconds to wait for a response condition to be satisfied. If the condition is
     * not satisfied before the timeout has elapsed, an exception is thrown. If the timeout is negative, the action is
     * fire and forget.
     *
     * @param id is the id of the request waiting for a response.
     * @param method is the method name of the request waiting for a response.
     * @param condition is a lambda specifying a condition that will indicate that the timeout has been met and can be
     * canceled.
     *
     * @return the condition value.
     */
    suspend fun waitForConditionOrTimeout(timeout: Long, id: Id, method: String, condition: () -> Boolean): Boolean
}
