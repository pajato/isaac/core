package com.pajato.isaac.core

import com.pajato.isaac.api.DocumentSymbolParams

/**
 * Declare the LSP textDocument/documentSymbol request operation.
 */
interface TextDocumentDocumentSymbol {
    /**
     * Execute the LSP textDocument/documentSymbol request message.
     *
     * @param[params] the optional query string
     * @param[timeout] the timeout value: <0 -> do not wait or cancel; 0 -> immediate cancel; >0 wait and then cancel
     * @param[responseHandler] A response handler (lambda) executed automagically.
     *
     * @since 0.9.21
     */
    fun execute(
        params: DocumentSymbolParams,
        timeout: Long = defaultTimeout,
        responseHandler: ResponseHandler
    )
}
