package com.pajato.isaac.core

import java.time.Instant
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

data class LogEntry(val timeStamp: Instant, val category: String, val jsonMessage: String, val context: String)

interface Logger {
    fun logMessage(category: String, message: String, scope: String = "")
    fun subscribe(handler: (LogEntry) -> Unit): Int
    fun unsubscribe(key: Int)
}

class CoreLogger : Logger {
    internal val handlerMap: MutableMap<Int, (LogEntry) -> Unit> = mutableMapOf()
    private var nextKey = 0

    override fun logMessage(category: String, message: String, scope: String) {
        val timeStamp: Instant = Instant.now()
        val entry = LogEntry(timeStamp, category, message, scope)

        fun hasSubscriber() = handlerMap.isNotEmpty()

        if (hasSubscriber()) runBlocking { sendToSubscribers(entry) }
    }

    override fun subscribe(handler: (LogEntry) -> Unit): Int {
        val key = nextKey++
        handlerMap[key] = handler
        return key
    }

    override fun unsubscribe(key: Int) { handlerMap.remove(key) }

    private fun getMessage(value: LogEntry): Flow<LogEntry> = flow { emit(value) }

    @OptIn(DelicateCoroutinesApi::class)
    private suspend fun sendToSubscribers(entry: LogEntry) {
        withContext(newSingleThreadContext("subscriber")) {
            launch { delay(1) }
            handlerMap.values.forEach { handler -> getMessage(entry).collect { entry -> handler(entry) } }
        }
    }
}
