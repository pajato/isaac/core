package com.pajato.isaac.core.client

import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.core.CoreLogger
import com.pajato.isaac.core.LanguageServerSpec
import com.pajato.isaac.core.Logger
import com.pajato.isaac.core.MessageRouter
import com.pajato.isaac.core.ProcessMessageRouter
import com.pajato.isaac.core.ProcessServerLauncher
import com.pajato.isaac.core.ServerLauncher
import com.pajato.isaac.core.getUniqueLanguageServerSpec

/**
 * A client object that streams messages to and from a process. See the Client interface documentation.
 *
 * @throws IllegalStateException when the given id and path do not generate a unique spec.
 */
class ProcessClient(
    id: String,
    override val params: InitializeParams = InitializeParams(capabilities = ClientCapabilities()),
    override val registryPath: String = defaultServerSpecPath,
    override val cachePath: String = "",
    override val logger: Logger = CoreLogger()
) : Client {
    override val spec: LanguageServerSpec = getUniqueLanguageServerSpec(id, registryPath)
    override val launcher: ServerLauncher = ProcessServerLauncher(this)
    override val router: MessageRouter = ProcessMessageRouter(this)
}
