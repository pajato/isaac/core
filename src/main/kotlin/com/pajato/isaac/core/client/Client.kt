package com.pajato.isaac.core.client

import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.core.LanguageServerSpec
import com.pajato.isaac.core.Logger
import com.pajato.isaac.core.MessageRouter
import com.pajato.isaac.core.ServerLauncher

/**
 * When the client interface is implemented in an application or library, then that library or application becomes a
 * client to an LSP server, as long as that server is properly configured in a server spec registry.
 */
interface Client {
    /**
     * A configuration entry for a server that complies with the Language Server Protocol specification, version 3.16
     * or greater.
     */
    val spec: LanguageServerSpec

    /**
     * The set of client configuration parameters that tell the server what the client can do, its capabilities.
     * During the server startup (start request), the server will respond to the client with its own set of
     * capabilities based on the client capabilities.
     */
    val params: InitializeParams

    /**
     * The logger shared by the launcher and router.
     */
    val logger: Logger

    /**
     * The object responsible for activating (registering and creating) a server. The client uses this launcher to
     * start a server process, for example.
     */
    val launcher: ServerLauncher

    /**
     * A mechanism the client uses to manage messages between the client and server. This includes
     * sending and receiving notification messages, sending request messages and receiving response messages. Sending
     * and receiving are asynchronous so the message router handles matching responses with requests.
     */
    val router: MessageRouter

    /**
     * An absolute file path to a language server spec registry (map) that associates a client/server id with its LSP
     * configuration spec.
     */
    val registryPath: String

    /**
     * An absolute file path to a cache that has captured messages keyed by a request. Used only for testing.
     */
    val cachePath: String
}

/**
 * Create a client object that allows the using tool or application to be an LSP client.
 *
 * @param id is the server spec id ussd to locate a server spec in a language server spec registry.
 * @param params is the set of initialization parameters specifying the client capabilities.
 * @param registryPath is an absolute path to a language server spec registry; defaults to the User's
 * "~/.lsp/servers.txt" file.
 * @param cachePath is the absolute path for a file that caches request, response and notification messages shared
 * between a client and server.
 *
 * @return an object tha implements the Client interface using the given id and registry to specify the object.
 *
 * @throws IllegalStateException when the given client id is not unique (or does not exist) in the given registry.
 */

fun Client(
    id: String,
    params: InitializeParams,
    registryPath: String = defaultServerSpecPath,
    cachePath: String = ""
): Client = ProcessClient(id, params, registryPath, cachePath)
