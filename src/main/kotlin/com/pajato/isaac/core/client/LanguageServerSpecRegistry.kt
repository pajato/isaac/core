package com.pajato.isaac.core.client

import com.pajato.isaac.core.Language
import com.pajato.isaac.core.LanguageServerSpec
import com.pajato.isaac.core.LanguageServerSpecSerializer
import java.io.File
import kotlinx.serialization.json.Json
import kotlinx.serialization.SerializationException

interface LanguageServerSpecRegistry {
    /**
     * The path to a file in which to persist the registered language servers. Defaults to the User's home directory:
     * ~/.lsp/servers.txt
     */
    val persistencePath: String

    /**
     * Return a map of all configuration specs that are language server specs.
     */
    fun getAll(): List<LanguageServerSpec>

    /**
     * Return a list of language server specs for the given language. The other arguments are convenience filters.
     */
    fun find(language: Language, id: String, version: String): List<LanguageServerSpec>
}

val defaultServerSpecPath = """${System.getProperty("user.home")}/.lsp/servers.txt"""

/**
 * Create a language server spec registry throwing an illegal argument exception when the persistence path argument
 * does not represent a valid file.
 *
 * @param persistencePath a valid path to the file where language server specs will be registered. The default is the
 * file path: "~/.lsp/servers.txt".
 *
 * @return an object that implements the language server spec registry interface.
 *
 * @throws IllegalArgumentException when the persistence path does not represent a valid file or cannot be created.
 * @throws SerializationException when an entry in the file cannot be loaded.
 */
fun LanguageServerSpecRegistry(persistencePath: String = defaultServerSpecPath): LanguageServerSpecRegistry =
    IsaacLanguageServerSpecRegistry(persistencePath)

internal class IsaacLanguageServerSpecRegistry(override val persistencePath: String) : LanguageServerSpecRegistry {
    override fun getAll(): List<LanguageServerSpec> = map.flatMap { it.value }

    override fun find(language: Language, id: String, version: String) =
        map[language]!!.filter { server ->
            (id.isEmpty() || id == server.id) && (version.isEmpty() || version == server.cacheDir)
        }

    private val map: MutableMap<Language, MutableList<LanguageServerSpec>> = mutableMapOf()

    init {
        val persistenceFile = File(persistencePath)

        fun persistenceFileExists(): Boolean = persistenceFile.let { it.exists() || it.createNewFile() }

        fun registerEntry(json: String) {
            val server = Json.decodeFromString(LanguageServerSpecSerializer, json)
            val list = map[server.language] ?: mutableListOf()

            list.add(server)
            map[server.language] = list
        }

        require(persistenceFile.isFile) { "The persistence file: $persistencePath must be a file!" }
        require(persistenceFileExists()) { "The persistence file: $persistencePath must exist or be creatable!" }
        persistenceFile.forEachLine { registerEntry(it) }
    }
}
