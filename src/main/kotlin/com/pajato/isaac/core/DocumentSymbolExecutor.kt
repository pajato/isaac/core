package com.pajato.isaac.core

import com.pajato.isaac.api.DocumentSymbolParams
import com.pajato.isaac.api.RequestMessage
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.core.client.Client

/**
 * A class that executes the textDocument/documentSymbol method.
 * @since 0.9.21
 */
class DocumentSymbolExecutor(val client: Client) : TextDocumentDocumentSymbol {
    override fun execute(
        params: DocumentSymbolParams,
        timeout: Long,
        responseHandler: (ResponseMessage) -> Unit
    ) {
        val router = client.router
        val method = "textDocument/documentSymbol"
        val subsystem = "doDocumentSymbol()"
        var haveSymbols = false

        fun processResponse(response: ResponseMessage) {
            val message = "$method response handler executed."
            responseHandler(response)
            client.logger.logMessage(clientInfoTag, message, subsystem)
            haveSymbols = true
        }

        fun sendRequestMessage(request: RequestMessage) =
            client.router.sendRequest(request, ::processResponse)

        client.cancelRequestMaybe(
            timeout, sendRequestMessage(RequestMessage(router.getNextRequestId(), method, params)), method
        ) { haveSymbols }
    }
}
