package com.pajato.isaac.core

import com.pajato.isaac.api.InitializeResult
import com.pajato.isaac.api.LogMessage
import com.pajato.isaac.api.Message
import com.pajato.isaac.api.NotificationMessage
import com.pajato.isaac.api.ResponseError
import com.pajato.isaac.api.ResponseMessage
import com.pajato.isaac.api.Result
import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.serverErrorTag
import com.pajato.isaac.serverNotificationTag
import com.pajato.isaac.serverResponseTag

internal fun String.toId(message: String): Int {
    val id = when (val matchResult = this.toRegex().find(message)) {
        null -> 0
        else -> matchResult.groupValues[1].toInt()
    }

    check(id > 0) { "Invalid JSON returned from language server." }
    return id
}

internal fun String.toIntPair(message: String, offset: Int) =
    when (val matchResult = this.toRegex().find(message.substring(offset))) {
        null -> Pair(-1, -1)
        else -> Pair(matchResult.groupValues[1].toInt(), matchResult.range.last + 1)
    }

internal fun StringBuilder.executeIfNotEmpty(handler: (String) -> Unit) {
    if (this.isNotEmpty()) handler(this.toString())
}

internal fun Message.routeMessage(router: MessageRouter) {
    fun routeNotification(message: NotificationMessage) {
        router.logMessage(serverNotificationTag, getMessageAsJson(message))
        if (message.method == "exit") router.initializeResult = null
    }

    fun routeResponse(message: ResponseMessage) {
        fun logErrorMessage() {
            val errorMessage = "Could not find the initialization handler for request with id: ${message.id}!"
            router.logMessage(serverErrorTag, errorMessage)
        }

        router.apply {
            logMessage(serverResponseTag, getMessageAsJson(message))
            responseHandlerMap[message.id?.toInt()]?.invoke(message) ?: logErrorMessage()
        }
    }

    return when (this) {
        is ResponseMessage -> routeResponse(this)
        is NotificationMessage -> routeNotification(this)
        is LogMessage -> { /* ignore */ }
        else -> throw IllegalStateException("Invalid message detected: ${getMessageAsJson(this)}")
    }
}

internal fun ResponseMessage.processResult(router: MessageRouter) {
    fun handleSuccess(result: Result) {
        val scope = "ResponseMessage.processResult"

        fun doInitialize() = router.run {
            initializeResult = result as InitializeResult
            logMessage(clientInfoTag, "Initialize response: Initialized!", scope)
        }

        router.apply {
            val name = result.javaClass.simpleName
            when (result) {
                is InitializeResult -> doInitialize()
                else -> logMessage(clientInfoTag, "Response result processed for class: $name", scope)
            }
        }
    }

    fun handleFailure(error: ResponseError) {
        val errorMessage = error.message
        val errorCode = error.code
        val message = "Uninitialized language server: error code: $errorCode, error message: $errorMessage!"
        router.logMessage(serverErrorTag, message, "BaseMessageRouter#Start()")
    }

    fun getServerErrorMessage() = "Server error: both response result and error properties are null!"

    when {
        result != null -> handleSuccess(result!!)
        error != null -> handleFailure(error!!)
        else -> throw IllegalStateException("Invalid response result: ${getServerErrorMessage()}")
    }
}
