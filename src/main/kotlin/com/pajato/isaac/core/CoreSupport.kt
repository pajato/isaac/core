package com.pajato.isaac.core

import com.pajato.isaac.api.ClientCapabilities
import com.pajato.isaac.api.InitializeParams
import com.pajato.isaac.api.LogMessage
import com.pajato.isaac.api.Message
import com.pajato.isaac.api.MessageSerializer
import com.pajato.isaac.api.ResponseMessage
import java.io.Reader
import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.produce
import kotlinx.coroutines.delay
import kotlinx.serialization.json.Json

typealias ResponseHandler = (ResponseMessage) -> Unit

const val delayInMilliseconds = 100L

internal fun getDefaultInitializeParams() = InitializeParams(capabilities = ClientCapabilities())

internal fun getMessageAsJson(message: Message): String = Json.encodeToString(MessageSerializer, message)

internal fun getMessageStream(reader: Reader): String {
    val charBuffer = CharArray(DEFAULT_BUFFER_SIZE)
    val size = reader.read(charBuffer, 0, DEFAULT_BUFFER_SIZE)
    return if (size > 0) StringBuilder().append(charBuffer, 0, size).toString() else ""
}

internal fun getMessagesFromStreamUsing(
    reader: Reader,
    messageStreamBuffer: StringBuilder,
    afterReadingMessagesHandler: () -> Unit,
): List<Message> {
    fun getMessagesIn(messageStream: String): List<Message> {
        val messageList = mutableListOf<Message>()

        tailrec fun getRestOfMessages(offset: Int, list: MutableList<Message>) {
            fun getNextMessage(): Int {
                val (contentLength, headerLength) = contentRE.toIntPair(messageStream, offset)
                val start = offset + headerLength
                val end = start + contentLength

                fun addMessage() {
                    fun getMessageFromJson(json: String): Message {
                        fun decodeJson(): Message = Json.decodeFromString(MessageSerializer, json)
                        fun log(exc: Exception) = LogMessage(exc.message ?: "No message available!")
                        return try { decodeJson() } catch (exc: Exception) { log(exc) }
                    }

                    list.add(getMessageFromJson(messageStream.substring(start, end)))
                }

                if (contentLength > 0) addMessage()
                return if (contentLength > 0) offset + headerLength + contentLength else messageStream.length
            }

            if (offset >= messageStream.length) return
            getRestOfMessages(getNextMessage(), list)
        }

        getRestOfMessages(0, messageList)
        return messageList
    }

    fun throwErrorMessage() { throw IllegalStateException("No message available to stream!") }

    while (reader.ready()) messageStreamBuffer.append(getMessageStream(reader).ifEmpty { throwErrorMessage() })
    afterReadingMessagesHandler()
    return getMessagesIn(messageStreamBuffer.toString())
}

@OptIn(ExperimentalCoroutinesApi::class)
@Suppress("BlockingMethodInNonBlockingContext")
fun CoroutineScope.produceClientNotificationOrRequestMessage(reader: Reader) =
    produce(CoroutineName("ClientMessageProducer")) {
        suspend fun waitForMessage() { delay(5L) }

        while (true) {
            while (!reader.ready()) waitForMessage()
            val message = reader.readText()
            send(message)
        }
    }

@OptIn(ExperimentalCoroutinesApi::class)
@Suppress("BlockingMethodInNonBlockingContext")
internal fun CoroutineScope.produceServerNotificationAndResponseMessages(reader: Reader) =
    produce(CoroutineName("ServerMessageProducer")) {
        suspend fun waitForMessage() { delay(5L) }

        while (true) {
            while (!reader.ready()) waitForMessage()
            val messageList = getMessagesFromStreamUsing(reader, StringBuilder()) { }
            messageList.forEach { send(it) }
        }
    }
