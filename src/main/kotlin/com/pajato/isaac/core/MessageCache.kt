package com.pajato.isaac.core

import com.pajato.isaac.clientErrorTag
import com.pajato.isaac.clientNotificationTag
import com.pajato.isaac.clientRequestTag
import com.pajato.isaac.core.client.Client
import java.io.File

internal const val partSep = "|||PART|||"
internal const val entrySep = "|||ENTRY|||"

class MessageCache(private val client: Client) {
    internal val cacheFile = File(client.cachePath)
    private val keyPrefix = "${client.spec.id}$partSep"
    private val keySuffix = "\n$partSep"
    private val entrySeparator = entrySep

    fun clear() = cacheFile.writeText("")

    fun addEntryToCache(logEntry: LogEntry) {
        fun saveClientRequestOrNotificationEntry() {
            if (cacheFile.length() > 0) cacheFile.appendText(entrySeparator)
            cacheFile.appendText("${keyPrefix}${filterJsonMessage(logEntry.jsonMessage)}$keySuffix")
        }

        when (logEntry.category) {
            clientRequestTag -> saveClientRequestOrNotificationEntry()
            clientNotificationTag -> saveClientRequestOrNotificationEntry()
            else -> cacheFile.appendText("${filterJsonMessage(logEntry.jsonMessage)}\n")
        }
    }

    fun copy() {
        val dir = client.spec.cacheDir
        val cacheDir = if (dir.isNullOrEmpty()) File("${System.getProperty("user.home")}/.lsp/") else File(dir)

        fun copyCache() = cacheFile.copyTo(File(cacheDir, cacheFile.name), overwrite = true)

        fun logError(message: String) = client.logger.logMessage(clientErrorTag, message, "MessageCache#copy()")

        if (cacheDir.exists()) copyCache() else logError("Invalid cache directory: ${client.spec.cacheDir} !")
    }

    private fun filterJsonMessage(jsonMessage: String): String {
        val userPath = System.getProperty("user.dir")
        val prefix = "Content-Length: "
        return jsonMessage.replace(Regex("$prefix\\d+"), "${prefix}0").replace(userPath, "/*")
            .replace(Regex(""""processId":\d+,"""), """"processId":0,""")
    }
}
