package com.pajato.isaac.core

enum class Language {
    ActionScript3, Ada, ANTLR, ApacheCamel, Ballerina, Bash, BibTeX, CSharp, C, CPluPlus, Clojure,
    CWL, Cobol, CSS_LESS_SASS, D, Dart, Delphi, Deno, Dockerfile, DreamMaker, Erlang, Elixir, Elm, FSharp, Flux,
    Fortran, Gauge, GLSL, Go, GraphQL, Groovy, Hack, Haskell, Haxe, HTML, Java, JavaScript, JSON, Julia, Kotlin, LateX,
    Lua, MOCA, OCaml, Nim, None, ObjectiveC, Perl, PHP, PL1, PowerShell, Puppet, PureScript, Python, QSharp, R, Racket,
    Red, Ruby, Rust, Scala, SPARQL, Swift, SystemVerilog, Terraform, Turtle, TypeScript, Vala, VimScript, XML, YAML,
    Xtext
}
