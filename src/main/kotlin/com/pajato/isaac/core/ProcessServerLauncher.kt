package com.pajato.isaac.core

import com.pajato.isaac.clientInfoTag
import com.pajato.isaac.core.client.Client
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.io.Reader
import java.io.Writer
import kotlin.io.path.createTempDirectory
import kotlin.io.path.createTempFile
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext

class ProcessServerLauncher(private val client: Client) : ServerLauncher {
    private var isActivated = false
    override val active: Boolean get() = isActivated

    internal val cache: MutableMap<String, List<String>> = mutableMapOf()
    internal val useCache = client.cachePath.isNotEmpty() && cacheFileIsNotEmpty()
    override val reader: Reader by lazy { if (useCache) getCacheReader() else getProcessReader() }
    override val writer: Writer by lazy { if (useCache) getCacheWriter() else getProcessWriter() }

    private lateinit var process: Process
    private val argList = mutableListOf<String>().apply { addAll(client.spec.command.split(" ")) }

    private val jobList = mutableListOf<Job>()

    init {
        fun loadCacheFromCacheFile() {
            val cacheFile = File(client.cachePath)
            val cacheFileError = "A valid cache file must exist. Aborting!"

            fun loadCacheWith(entry: String) {
                fun loadRequest(id: String, request: String, messages: List<String>) {
                    cache["$id:$request".filter { char -> !char.isWhitespace() }] = messages
                }

                fun loadCacheWith(parts: List<String>) = when (parts.size) {
                    3 -> loadRequest(parts[0], parts[1], parts[2].split("\n"))
                    2 -> loadRequest(parts[0], parts[1], listOf())
                    1 -> { }
                    else -> throw IllegalStateException("""Invalid cache entry detected: "$entry"!""")
                }

                loadCacheWith(entry.split(partSep))
            }

            check(cacheFile.isFile && cacheFile.exists() && cacheFile.length() > 0) { cacheFileError }
            cacheFile.readText().split(entrySep).forEach { entry -> loadCacheWith(entry) }
        }

        check(useCache || client.spec.command.isNotEmpty()) { "The server command name must be non-empty!" }
        if (useCache) loadCacheFromCacheFile()
    }

    override fun activate(vararg extraArgs: String) {
        fun launchFromServerMessageReader(client: Client): Job {
            suspend fun readMessagesUsingChannel(scope: CoroutineScope) {
                val message = scope.produceServerNotificationAndResponseMessages(client.launcher.reader)
                while (client.launcher.active) { message.receive().also { it.routeMessage(client.router) } }
                scope.cancel()
            }

            return CoroutineScope(Dispatchers.Default).launch(Dispatchers.IO) { readMessagesUsingChannel(this) }
        }

        fun launchProcessReader() {
            fun getErrorMessage(exc: Exception) =
                "Could not start the server with id: ${client.spec.id}. Failed with: ${exc.message}!"

            argList.addAll(extraArgs)
            try { startProcess() } catch (exc: Exception) { throw IllegalStateException(getErrorMessage(exc)) }
            jobList.add(launchFromServerMessageReader(client))
        }

        fun launchCacheReaders() {
            isActivated = true
            jobList.add(launchFromServerMessageReader(client))
            jobList.add(launchToServerMessageReader())
        }

        if (useCache) launchCacheReaders() else launchProcessReader()
    }

    override fun deactivate() {
        fun close() = runBlocking {
            withContext(Dispatchers.IO) { reader.close() }
            withContext(Dispatchers.IO) { writer.close() }
            delay(1L)
        }

        if (this::process.isInitialized) process.destroy()
        isActivated = false
        jobList.forEach { it.cancel() }
        close()
    }

    private fun getProcessReader() = BufferedReader(InputStreamReader(process.inputStream))
    private fun getProcessWriter() = BufferedWriter(OutputStreamWriter(process.outputStream))

    private fun startProcess() {
        process = ProcessBuilder(argList)
            .directory(File("${File(client.registryPath).parentFile.absolutePath}/scripts/"))
            .redirectErrorStream(true)
            .start()
        isActivated = process.isAlive
    }

    private fun cacheFileIsNotEmpty() = File(client.cachePath).length() > 0

    private val requestFile: File = createTempFile(createTempDirectory()).toFile()
    private fun getCacheWriter() = BufferedWriter(FileWriter(requestFile))

    private val notificationAndResponseFile: File = createTempFile(createTempDirectory()).toFile()
    private fun getCacheReader() = BufferedReader(FileReader(notificationAndResponseFile))

    private fun launchToServerMessageReader(): Job {
        val scope = CoroutineScope(Dispatchers.Default)
        val fileWriter = BufferedWriter(FileWriter(notificationAndResponseFile))
        val fileReader = BufferedReader(FileReader(requestFile))

        suspend fun handleRequestMessages(scope: CoroutineScope) {
            val message = scope.produceClientNotificationOrRequestMessage(fileReader)

            fun handleRequestMessage(message: String) {
                val dirPrefix = System.getProperty("user.dir")
                val cacheKey = "${client.spec.id}:$message".filter { !it.isWhitespace() }
                    .replace(dirPrefix, "/*")
                    .replace(Regex("Content-Length:\\d+"), "Content-Length:0")
                    .replace(Regex("""\d\d\d\d\d"""), "0")

                fun sendListMaybe(messageList: List<String>) {
                    fun getFormattedMessage(message: String) = "Content-Length: ${message.length}\r\n\r\n$message"

                    if (messageList.isEmpty()) return
                    fileWriter.apply { write(""); flush() }
                    messageList.forEach { message -> fileWriter.append(getFormattedMessage(message)) }
                    fileWriter.flush()
                }

                cache[cacheKey]?.let { sendListMaybe(it) } ?: run { /* Ignore (but log) missed cache keys. Instead,
                    rely on short timeouts to force errors. Doing so simplifies working with coroutines. */
                    val errorMessage = "Dirty cache miss! Cache key: $cacheKey, Cache keys: ${cache.keys}"
                    client.logger.logMessage(clientInfoTag, errorMessage, "launchToServerMessageReader()")
                }
            }

            while (active) { handleRequestMessage(message.receive()) }
        }

        return scope.launch(Dispatchers.IO) { handleRequestMessages(this) }
    }
}
