import org.gradle.api.publish.maven.MavenPom
import org.gradle.api.publish.maven.MavenPomDeveloper
import org.gradle.api.publish.maven.MavenPomDeveloperSpec
import org.gradle.api.publish.maven.MavenPomOrganization

object Publish {
    const val GROUP = "com.pajato.isaac"
    private const val POM_DESCRIPTION = "Kotlin/Language Server Protocol SDK: Core"
    private const val POM_DEVELOPER_ID = "pajatopmr"
    private const val POM_DEVELOPER_NAME = "Paul Michael Reilly"
    private const val POM_ORGANIZATION_NAME = "Pajato Technologies LLC"
    private const val POM_ORGANIZATION_URL = "https://pajato.com/"
    private const val POM_NAME = "isaac-core"
    private const val POM_LICENSE_DIST = "repo"
    private const val POM_LICENSE_NAME = "The GNU Lesser General Public License, Version 3"
    private const val POM_LICENSE_URL = "https://www.gnu.org/copyleft/lesser.html"
    private const val POM_SCM_CONNECTION = "scm:git:https://gitlab.com/pajato/isaac/api.git"
    private const val POM_SCM_DEV_CONNECTION = "scm:git:git@gitlab.com:pajato/isaac/api.git"
    private const val POM_SCM_URL = "https://gitlab.com/pajato/isaac/api"
    private const val POM_URL = "https://gitlab.com/pajato/isaac/api"

    fun populatePom(pom: MavenPom) {
        fun populatePomBasic() = pom.apply {
            name.set(POM_NAME)
            description.set(POM_DESCRIPTION)
            url.set(POM_URL)
        }

        fun populatePomLicenses() = pom.apply {
            licenses {
                license {
                    name.set(POM_LICENSE_NAME)
                    url.set(POM_LICENSE_URL)
                    distribution.set(POM_LICENSE_DIST)
                }
            }
        }

        fun populatePomScm() = pom.apply {
            scm {
                url.set(POM_SCM_URL)
                connection.set(POM_SCM_CONNECTION)
                developerConnection.set(POM_SCM_DEV_CONNECTION)
            }
        }

        fun populatePomDevelopers() = pom.apply {
            fun populatePomDevelopers(spec: MavenPomDeveloperSpec) = pom.apply {
                fun populatePomDeveloper(dev: MavenPomDeveloper) = pom.apply {
                    fun populatePomOrganization(org: MavenPomOrganization) {
                        org.name.set(POM_ORGANIZATION_NAME)
                        org.url.set(POM_ORGANIZATION_URL)
                    }

                    dev.id.set(POM_DEVELOPER_ID)
                    dev.name.set(POM_DEVELOPER_NAME)
                    organization { populatePomOrganization(this) }
                }

                spec.developer { populatePomDeveloper(this) }
            }

            developers { populatePomDevelopers(this) }
        }

        populatePomBasic()
        populatePomLicenses()
        populatePomScm()
        populatePomDevelopers()
    }
}
