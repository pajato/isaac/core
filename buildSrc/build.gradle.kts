// SPDX-License-Identifier: LGPL-3.0-or-later

plugins { `kotlin-dsl` }

repositories { mavenCentral() }

dependencies { }

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}


