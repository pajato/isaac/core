# core

The Language Server Protocol (LSP) Kotlin SDK Core. A layer providing support for LSP client, server and tool services on top of the sibling API layer.

This repo/project provides a Kotlin SDK component containing the core interfaces and related functions, classes and objects supporting development of Microsoft's Language Server Protocol clients and servers.

## Overview

The component is based on LSP specification version 3.16.x. That specification identifies a number of core services in the way of requests and responses. This component provides SDK support for those services. It is built on top of a set of an API sibling project.



## Support

If you encounter a bug, feel like an important feature is missing or generally have an observation to make, please file an issue on this repo.


## Roadmap

tbd

## Contributing

See the separate doc: CONTRIBUTING.md

## Authors and acknowledgment

tbd

## License

This project is licensed using the GPL. See the separate document LICENSE for details.

## Project status

Active and early stages of development.
